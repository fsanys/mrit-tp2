# mrit-architecture-applicative

## Develop a single page application with Flask and Vue.js

### How to build the project

1. Clone

2. Run the server-side Flask app in one terminal window:

```sh
cd backend
pip3 install -r requirements.txt
python3.6 app.py
```

Navigate to [http://@ip_server:8080](http://@ip_server:8080)

1. Run the client-side Vue app in a different terminal window:

```sh
cd frontend
npm install --global yarn
yarn install
yarn serve --port 8081
```

Navigate to [http://@ip_server:8081](http://@ip_server:8081)

